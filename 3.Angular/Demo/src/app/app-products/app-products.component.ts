import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-products',
  templateUrl: './app-products.component.html',
  styleUrl: './app-products.component.css'
})
export class  ProductsComponent implements OnInit {
   
  products: any;
  emailId: any;
  selectedProduct : any;
  cartProducts : any;


  //AddToCart
  // cartItems: any;

  constructor(private service: EmpService) {

    //AddToCart
    // this.cartItems = [];

    this.emailId = localStorage.getItem('emailId');


    this.products = [
      {id:1001, name:"Naruto",   price:14999.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/1001.jpg"},
      {id:1002, name:"One piece", price:24999.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10002.jpg"},
      {id:1003, name:"demon slayer",  price:34999.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10003.jpg"},
      {id:1004, name:"Hulk",  price:44999.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10004.jpg"},
      {id:1005, name:"Averages",    price:54999.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10005.jpg"},
      {id:1006, name:"Anit-man",    price:64999.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10006.jpg"}
    ];
  }
  ngOnInit() {
  }
  addToCart(product:any){
    this.service.addToCart(product);
  
    // this.cartItems.push(product);
    // localStorage.setItem("cartItems", JSON.stringify(this.cartItems));
  }

}
 