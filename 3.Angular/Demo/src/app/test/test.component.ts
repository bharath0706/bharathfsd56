import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
  id: number;
  name:string;
  avg:number;

  address:any;
  hobbies:any;
  
 

  constructor(){
    //alert("constructor invovked")
    this.id = 101;
    this.name = 'bharath';
    this.avg = 67.7;
     
     this.address = {
      streetNo:101,
      city:'Hyderabad',
      state:'Telangana'
     };

     this.hobbies =['sleeping','Eating'];
  }
ngOnInit(){
  
}
}
